/* Trident CMS by Isaiah Gilliland 2012
 *      A simple yet elegant CMS system designed to be a professional's
 *      system and work mostly as glue for all the content in a website.
 *      No want or need for an over elaborate system or hand holding, 
 *      just something that does the job. 
 * */

import php.Lib;
import php.Web;
import php.io.File;
import php.FileSystem;
import php.db.Mysql;
typedef Plb = php.Lib; //changing the names to something more convenient
typedef Pwb = php.Web;

//All the types to be used by the system.
typedef Page = {
	var name : String;
	var template : String;
	var content : String;
	var blog : Int;
	var blognum : Int;
}
typedef Article = {
	var date : String;
	var title : String;
	var author : String;
	var content : String;
}
typedef User = {
	var name : String;
	var pass : String;
	var email : String;
}

class Config {
	static public var dbhost : String;
	static public var dbuser : String;
	static public var dbpass : String;
	static public var dbname : String;
	static public var sitename : String;
	static public var siteurl : String;
	
	static public function set(host,user,pass,name,stname,sturl) {
		dbhost = host;
		dbuser = user;
		dbpass = pass;
		dbname = name;
		sitename = stname;
		siteurl = sturl;
	}
}

//administration/authorization stuff
class Admin {
	
	static public function launch(){
		
	}
	static public function adminLogin(func : Void -> Void){
		
	}
	static public function checkAdmin() {
			
	}
}

class Trident {	
	static function main() { 
		var params = Pwb.getParams();
		//check if installation has been done
		if(!php.FileSystem.exists("config.xml")){
			if(params.exists('dbhost'))
				makeInstall(params);
			else
				launchInstall();
		} else {
			//get configuration
			getCfg();
			if(params.exists('admin')){
				Admin.launch();
			} else {
				var name = params.exists('p') ? params.get('p') : 'home';
				launchPage(name);
			}
		}
	}
	static function systemWrap(content, ?title){
		var beginning = '<!DOCTYPE html>\n<html>\n<head>\n';
		beginning += '<link rel="stylesheet" type="text/css" href="scripts/system.css" />';
		beginning += '\n<script type="text/javascript" src="scripts/system.js"></script>';
		beginning += '\n</head>\n<body>\n<div id="syspage">\n';
		beginning += '<h1>' + title + '</h1>\n';
		var ending = '\n<div id="footer">Trident CMS 2012</div>\n</div>\n</body>';
		
		return beginning + content + ending;
	}
	//configuration stuff
	static function getCfg(){
		try {
			//getting config xml string and passing it on
			parseCfg(php.io.File.getContent("config.xml"));
						
		} catch(msg : String){
			trace("Could not read cfg file: " + msg);
		}
		
	}
	static function parseCfg(file){
		var xml = Xml.parse(file);
		var data = new haxe.xml.Fast(xml);	
		//store it in a static config var
		Config.set(
			data.node.database.node.host.innerData,
			data.node.database.node.username.innerData,
			data.node.database.node.password.innerData,
			data.node.database.node.name.innerData,
			data.node.site.node.name.innerData,
			data.node.site.node.url.innerData
		);
	}
	
	//normal page stuff
	static function launchPage(name){
		var page = sqlGetPage(name);
		var template = getTemplate(page);
		//check if it's a blog
		if(page.blog != 1){
			Plb.print(buildPage(page,template));
		}
	}
	static function sqlGetPage(name):Page{
		var cnx = php.db.Mysql.connect({
			host : Config.dbhost,
			port : 3306,
			user : Config.dbuser,
			pass : Config.dbpass,
			socket : null,
			database : Config.dbname
		});
		//get page and store in Page type
		var r = cnx.request("SELECT * FROM pages WHERE name = "+cnx.quote(name));
		cnx.close();
		var page : Page = {
			name : r.getResult(1), 
			template : r.getResult(3), 
			content : r.getResult(2), 
			blog : r.getIntResult(4), 
			blognum : r.getIntResult(5)};
		return page;
	}
	static function getTemplate(page):String{
		var template = php.io.File.getContent(page.template + "index.html");
		return template;
	}
	static function buildPage(page,template):String{
		//adding content
		var data = StringTools.replace(template, ">>content<<", page.content);
		//adding relevent paths
		data = StringTools.replace(data, ">>temppath<<", page.template);
		data = StringTools.replace(data, ">>pagename<<", page.name);
		data = StringTools.replace(data, ">>sitename<<", Config.sitename);
		data = StringTools.replace(data, ">>siteurl<<", Config.siteurl);
		return data;
	}

	//installation stuff
	static function launchInstall(){
		var content = '<form method="POST" action="index.php" >\n';
		content += '<h3>Please enter the Trident CMS mysql database information.</h3>\n';
		content += '<label>Database Host:</label><input name="dbhost" type="text" placeholder="localhost" required>\n';
		content += '<label>Database Name:</label><input name="dbname" type="text" required>\n';
		content += '<label>Database Username:</label><input name="dbuser" type="text" required>\n';
		content += '<label>Database Password:</label><input name="dbpass" type="text" required>\n';
		content += '<h3>Please enter this site\'s information below.</h3>\n';
		content += '<label>Site Name:</label><input name="sitename" type="text" required>\n';
		content += '<label>Site Url:</label><input name="siteurl" type="text" value="http://" required>\n';
		content += '<h3>Please enter your desired administration credentials.</h3>\n';
		content += '<label>Username:</label><input name="adminuser" type="text" placeholder="admin" required>\n';
		content += '<label>Password:</label><input name="adminpass" type="text" required>\n';
		content += '<label>Email:</label><input name="adminemail" type="email" required>\n';
		content += '<input type="submit" name="install" /></form>';
		
		Plb.print(systemWrap(content, "Installation"));
	}
	static function makeInstall(params) {
		if(php.FileSystem.exists("config.xml")){
			Plb.print("Installation already executed, administer has been notified with IP");
		} else {
			Plb.print("Installation is on the way " + params.get('adminuser') + "\n");
			cfgInstall(params);
			sqlInstall(params);
			Pwb.redirect(params.get('siteurl'));
		}
	}
	static function cfgInstall(params){
		var cfg = "<database>\n\t<host>" + params.get('dbhost') + "</host>\n";
		cfg += "\t<name>" + params.get('dbname') + "</name>\n";
		cfg += "\t<username>" + params.get('dbuser') + "</username>\n";
		cfg += "\t<password>" + params.get('dbpass') + "</password>\n";
		cfg += "</database>\n";
		cfg += "<site>\n\t<name>" + params.get('sitename') + "</name>\n";
		cfg += "\t<url>" + params.get('siteurl') + "</url>\n</site>";
		
		var file = php.io.File.write("config.xml", false);
		file.writeString(cfg);
		file.close();
	}
	static function sqlInstall(params){
		//setting up connection
		var cnx = php.db.Mysql.connect({
			host : params.get('dbhost'),
			port : 3306,
			user : params.get('dbuser'),
			pass : params.get('dbpass'),
			socket : null,
			database : params.get('dbname')
		});
		//if pages exists, make it and add home page		
		Plb.print("\nmaking pages table");
		cnx.request("CREATE TABLE IF NOT EXISTS pages (
					 id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					 name VARCHAR(20),
					 content MEDIUMTEXT,
					 template VARCHAR(40),
					 blog BOOL,
					 blognum INT(1));"
		);
		cnx.request(
		"INSERT IGNORE INTO pages SET 
		id = 0, name = 'home',
		content = 'This is the default home page for Trident CMS',
		template = 'template/default/', blog = FALSE, blognum = 0;"
		);		
		cnx.close();
	}
	
	
} 


