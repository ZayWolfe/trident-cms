<?php

class Trident {
	public function __construct(){}
	static function main() {
		$params = php_Web::getParams();
		if(!file_exists("config.xml")) {
			if($params->exists("dbhost")) {
				Trident::makeInstall($params);
			} else {
				Trident::launchInstall();
			}
		} else {
			Trident::getCfg();
			if($params->exists("admin")) {
				Admin::launch();
			} else {
				$name = (($params->exists("p")) ? $params->get("p") : "home");
				Trident::launchPage($name);
			}
		}
	}
	static function systemWrap($content, $title) {
		$beginning = "<!DOCTYPE html>\x0A<html>\x0A<head>\x0A";
		$beginning .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"scripts/system.css\" />";
		$beginning .= "\x0A<script type=\"text/javascript\" src=\"scripts/system.js\"></script>";
		$beginning .= "\x0A</head>\x0A<body>\x0A<div id=\"syspage\">\x0A";
		$beginning .= "<h1>" . $title . "</h1>\x0A";
		$ending = "\x0A<div id=\"footer\">Trident CMS 2012</div>\x0A</div>\x0A</body>";
		return $beginning . $content . $ending;
	}
	static function getCfg() {
		try {
			Trident::parseCfg(php_io_File::getContent("config.xml"));
		}catch(Exception $�e) {
			$_ex_ = ($�e instanceof HException) ? $�e->e : $�e;
			if(is_string($msg = $_ex_)){
				haxe_Log::trace("Could not read cfg file: " . $msg, _hx_anonymous(array("fileName" => "Trident.hx", "lineNumber" => 105, "className" => "Trident", "methodName" => "getCfg")));
			} else throw $�e;;
		}
	}
	static function parseCfg($file) {
		$xml = Xml::parse($file);
		$data = new haxe_xml_Fast($xml);
		Config::set($data->node->resolve("database")->node->resolve("host")->getInnerData(), $data->node->resolve("database")->node->resolve("username")->getInnerData(), $data->node->resolve("database")->node->resolve("password")->getInnerData(), $data->node->resolve("database")->node->resolve("name")->getInnerData(), $data->node->resolve("site")->node->resolve("name")->getInnerData(), $data->node->resolve("site")->node->resolve("url")->getInnerData());
	}
	static function launchPage($name) {
		$page = Trident::sqlGetPage($name);
		$template = Trident::getTemplate($page);
		if($page->blog !== 1) {
			php_Lib::hprint(Trident::buildPage($page, $template));
		}
	}
	static function sqlGetPage($name) {
		$cnx = php_db_Mysql::connect(_hx_anonymous(array("host" => Config::$dbhost, "port" => 3306, "user" => Config::$dbuser, "pass" => Config::$dbpass, "socket" => null, "database" => Config::$dbname)));
		$r = $cnx->request("SELECT * FROM pages WHERE name = " . $cnx->quote($name));
		$cnx->close();
		$page = _hx_anonymous(array("name" => $r->getResult(1), "template" => $r->getResult(3), "content" => $r->getResult(2), "blog" => $r->getIntResult(4), "blognum" => $r->getIntResult(5)));
		return $page;
	}
	static function getTemplate($page) {
		$template = php_io_File::getContent($page->template . "index.html");
		return $template;
	}
	static function buildPage($page, $template) {
		$data = str_replace(">>content<<", $page->content, $template);
		$data = str_replace(">>temppath<<", $page->template, $data);
		$data = str_replace(">>pagename<<", $page->name, $data);
		$data = str_replace(">>sitename<<", Config::$sitename, $data);
		$data = str_replace(">>siteurl<<", Config::$siteurl, $data);
		return $data;
	}
	static function launchInstall() {
		$content = "<form method=\"POST\" action=\"index.php\" >\x0A";
		$content .= "<h3>Please enter the Trident CMS mysql database information.</h3>\x0A";
		$content .= "<label>Database Host:</label><input name=\"dbhost\" type=\"text\" placeholder=\"localhost\" required>\x0A";
		$content .= "<label>Database Name:</label><input name=\"dbname\" type=\"text\" required>\x0A";
		$content .= "<label>Database Username:</label><input name=\"dbuser\" type=\"text\" required>\x0A";
		$content .= "<label>Database Password:</label><input name=\"dbpass\" type=\"text\" required>\x0A";
		$content .= "<h3>Please enter this site's information below.</h3>\x0A";
		$content .= "<label>Site Name:</label><input name=\"sitename\" type=\"text\" required>\x0A";
		$content .= "<label>Site Url:</label><input name=\"siteurl\" type=\"text\" value=\"http://\" required>\x0A";
		$content .= "<h3>Please enter your desired administration credentials.</h3>\x0A";
		$content .= "<label>Username:</label><input name=\"adminuser\" type=\"text\" placeholder=\"admin\" required>\x0A";
		$content .= "<label>Password:</label><input name=\"adminpass\" type=\"text\" required>\x0A";
		$content .= "<label>Email:</label><input name=\"adminemail\" type=\"email\" required>\x0A";
		$content .= "<input type=\"submit\" name=\"install\" /></form>";
		php_Lib::hprint(Trident::systemWrap($content, "Installation"));
	}
	static function makeInstall($params) {
		if(file_exists("config.xml")) {
			php_Lib::hprint("Installation already executed, administer has been notified with IP");
		} else {
			php_Lib::hprint("Installation is on the way " . $params->get("adminuser") . "\x0A");
			Trident::cfgInstall($params);
			Trident::sqlInstall($params);
			php_Web::redirect($params->get("siteurl"));
		}
	}
	static function cfgInstall($params) {
		$cfg = "<database>\x0A\x09<host>" . $params->get("dbhost") . "</host>\x0A";
		$cfg .= "\x09<name>" . $params->get("dbname") . "</name>\x0A";
		$cfg .= "\x09<username>" . $params->get("dbuser") . "</username>\x0A";
		$cfg .= "\x09<password>" . $params->get("dbpass") . "</password>\x0A";
		$cfg .= "</database>\x0A";
		$cfg .= "<site>\x0A\x09<name>" . $params->get("sitename") . "</name>\x0A";
		$cfg .= "\x09<url>" . $params->get("siteurl") . "</url>\x0A</site>";
		$file = php_io_File::write("config.xml", false);
		$file->writeString($cfg);
		$file->close();
	}
	static function sqlInstall($params) {
		$cnx = php_db_Mysql::connect(_hx_anonymous(array("host" => $params->get("dbhost"), "port" => 3306, "user" => $params->get("dbuser"), "pass" => $params->get("dbpass"), "socket" => null, "database" => $params->get("dbname"))));
		php_Lib::hprint("\x0Amaking pages table");
		$cnx->request("CREATE TABLE IF NOT EXISTS pages (\x0A\x09\x09\x09\x09\x09 id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\x0A\x09\x09\x09\x09\x09 name VARCHAR(20),\x0A\x09\x09\x09\x09\x09 content MEDIUMTEXT,\x0A\x09\x09\x09\x09\x09 template VARCHAR(40),\x0A\x09\x09\x09\x09\x09 blog BOOL,\x0A\x09\x09\x09\x09\x09 blognum INT(1));");
		$cnx->request("INSERT IGNORE INTO pages SET \x0A\x09\x09id = 0, name = 'home',\x0A\x09\x09content = 'This is the default home page for Trident CMS',\x0A\x09\x09template = 'template/default/', blog = FALSE, blognum = 0;");
		$cnx->close();
	}
	function __toString() { return 'Trident'; }
}
