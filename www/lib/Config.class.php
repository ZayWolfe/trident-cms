<?php

class Config {
	public function __construct(){}
	static $dbhost;
	static $dbuser;
	static $dbpass;
	static $dbname;
	static $sitename;
	static $siteurl;
	static function set($host, $user, $pass, $name, $stname, $sturl) {
		Config::$dbhost = $host;
		Config::$dbuser = $user;
		Config::$dbpass = $pass;
		Config::$dbname = $name;
		Config::$sitename = $stname;
		Config::$siteurl = $sturl;
	}
	function __toString() { return 'Config'; }
}
